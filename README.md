# Формат сдачи материалов и оценивание
## Список материалов
1. Скриншоты команд docker version и docker compose version.

![docker1.png](docker1.png)

2. Скриншоты команд docker compose ps с сервисами

* ldap,

![docker1.png](docker1.png)

* ldap и ldap_admin,

![docker2.png](docker2.png)

* ldap, ldap_admin, gitlab,

![docker3.png](docker3.png)

* ldap, ldap_admin, gitlab, rocketchat и mongodb.

![docker4.png](docker4.png)

3. Скриншот выполненного входа в веб-интерфейс phpLDAPadmin.

![docker5.png](docker5.png)

4. Скриншот результата выполненного входа в GitLab.

![docker6.png](docker6.png)

5. Скриншот окна входа в GitLab с двумя входами: Standard и LDAP.

![docker7.png](docker7.png)

6. Скриншот окна веб-интерфейса ldap_admin с созданной учётной записью.

![docker8.png](docker8.png)

7. Скриншот выполненного входа в GitLab учётной записью из LDAP-каталога.

![docker9.png](docker9.png)

8. Скриншот выполненного входа в Rocket Chat.

![docker10.png](docker10.png)

9. Скриншот окна выполненного входа в Rocket Chat с использованием учётной записи из LDAP-каталога.

![docker11.png](docker11.png)

10. Ссылка на Git-репозиторий с файлом compose.yaml и всеми скриншотами финальной работы.

[Ссылка](https://gitlab.com/kav5364737/docker-final.git)